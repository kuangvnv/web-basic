import { gql } from 'apollo-boost'

export const CREATE_REGISTER = gql`
mutation CreateUser($data: UserInput!) {
  createUser(data: $data) {
    id
  }
}
`


export const USER_LOGIN = gql`
mutation Login($data: UserAuthInput!) {
  login(data: $data) {
    data {
      id
      firstName
      lastName
      phone
      createdAt
    }
    refreshToken
    accessToken
  }
}
`