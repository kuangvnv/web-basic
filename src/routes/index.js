import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";

import LoginPage from "../pages/login/LoginPage";
import Register from "../pages/register/Register"
import Deshborad from "../pages/deshborad/Deshborad"

import Consts from "../consts"

function Index() {
  return (
    <Router>
      <Routes>
        <Route path="/"  element={<LoginPage />} />
        <Route path={Consts.USER_REGISTER}  element={<Register />} />
        <Route path={Consts.PAGE_DESHBOARD}  element={<Deshborad />} />
        <Route path={Consts.USER_LOGIN}  element={<LoginPage />} />
        
        
        
      </Routes>
    </Router>
  );
}

export default Index;
