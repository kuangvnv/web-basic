import React from 'react'
import {Route, Redirect} from 'react-router-dom'

import {useAuth} from '../helpers/index'
// import Header from '../components/Header'

/**
 *
 * return authenticated header & component
 */
function PrivateRoute ({component: Component, headerTitle, ...rest}) {
  const isAuthenticated = useAuth()

  // if not authenticated, redirect to "/"
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated == false
          ? <div>
            {/* <Header isAuthenticated headerTitle={headerTitle} /> */}
            <Component {...props} />
          </div>
          : <Component {...props} />}
          // : <Redirect to='/' />}
    />
  )
}

export default PrivateRoute
