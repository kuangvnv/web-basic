import React, { useState,useEffect } from 'react'
import { Link } from "react-router-dom";
import Swal from 'sweetalert2'
import { useNavigate } from "react-router-dom";
import Consts from "../../consts"
import { useMutation } from "@apollo/react-hooks";
import {USER_DATA} from "../../consts"
import Form from 'react-bootstrap/Form'
import { Button, Col,  } from 'react-bootstrap';
import logoJCD from '../../images/logoJCD.png'

import { USER_LOGIN } from "../../apollo/user/Mutation"




function Deshborad() {

    const navigate = useNavigate();

    const [userInfo, setUserInfo] = useState()

    useEffect(()=>{
        getQueryData()
    },[])


    const getQueryData = async () => {
        let _userDataParse = localStorage.getItem(USER_DATA);
        let _userData;

        if (_userDataParse) {
            _userData = JSON.parse(_userDataParse);
        }

        console.log("_userData-->",_userData)

        setUserInfo(_userData)
    }




    return (
        <div 
        style={{
            margin: 0,
            padding: 0,
            backgroundColor: "#3248a8",
            minHeight: "100vh"
        }}
        >

            <center>
                <img
                    className="img"
                    src={logoJCD}
                    alt="First slide"
                    style={{
                        marginTop: 20,
                        width: 300,
                        height: 300
                    }}
                />

                <p 
                style={{ 
                    marginTop:"20px",
                    fontSize:"30px",
                    color: "#fff"}}
                >ຍິນຕ້ອນຮັບເຂົ້າສູ່ງານເຈົ້າແຄນເດບຄັ້ງທີ2</p>

            </center>

            <p 
            style={{
                textAlign: "center",
                fontSize:"25px",
                color: "#fff"
            }}
            >ສະບາຍດີ, {userInfo?.firstName + " " + userInfo?.lastName}</p>
            <center>

                <Button as={Col} className='save-button' size="lg" onClick={async () => {
                    Swal.fire({
                        title: "ອອກຈາກລະບົບ!",
                        text: "ທ່ານຕ້ອງການອອກຈາກລະບົບແທ້ຫລືບໍ?",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'ຍົກເລິກ',
                        confirmButtonText: 'ອອກຈາກລະບົບ'
                    }).then(async (result) => {
                        if (result.isConfirmed) {
                            await localStorage.clear();
                            navigate("/");
                        }
                    });

                }}
                style={{
                    backgroundColor: "#fff",
                    color: "#3248a8",
                    fontWeight:"bold",
                    border: 0,
                    width: "15%"
                }}
                >ອອກຈາກລະບົບ</Button>
            </center>



        </div>
    )
}

export default Deshborad
