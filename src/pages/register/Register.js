import React, { useState } from 'react'
import { useMutation } from "@apollo/react-hooks";
import Form from 'react-bootstrap/Form'
import { Formik } from 'formik'
import Swal from 'sweetalert2'
import { Button, Col, } from 'react-bootstrap';
import logoJCD from '../../images/logoJCD.png'
import './Index.css'
import { Link } from "react-router-dom";
import Consts from "../../consts"

import { CREATE_REGISTER } from "../../apollo/user/Mutation"

function Register() {
    // ການເອີ້ນໃຊ້ route create
    const [createRegister] = useMutation(CREATE_REGISTER)

    // function
    // ສ້າງ function ເພື່ອສົ່ງຂໍ້ມູນໄປບັນທຶກໃນຖານຂໍ້ມູນ
    const _createRegister = async (values) => {
        try {
            let create = await createRegister({
                variables: {
                    data: values
                }
            })

            if (create) {
                Swal.fire({
                    icon: 'success',
                    title: 'ລົງທະບຽນສຳເລັດ',
                    showConfirmButton: false,
                    timer: 1500
                })
                window.location.reload()
            }

        }
        catch (err) {
            Swal.fire({
                icon: 'error',
                title: 'ມີຫຍັງມີຜິດພາດ',
                text: 'ບໍ່ສາມາດລົງລະບຽນໄດ້ ກະລຸນາລອງໃໝ່ອີກຄັ້ງ!',
                showConfirmButton: false,
            })
        }
    }

    return (
        <div className='login-body'>
            <div className="flex-container">
                <div className="flex-left">
                    <h2>Jao Can Dev - ເຈົ້າແຄນເດບ ຄັ້ງທີ 2</h2>

                    <div className='form-body'>
                        <h3>Register</h3>
                        <p>ລົງທະບຽນ ເພື່ອເຂົ້າລະບົບ</p>
                        <Formik
                            initialValues={{}}
                            validate={(values) => {
                                const errors = {};
                                if (!values.firstName) {
                                    errors.firstName = "ກະລຸນປ້ອນຊື່";
                                }

                                if (!values.lastName) {
                                    errors.lastName = "ກະລຸນາປ້ອນນາມສະກຸນ";
                                }
                                if (!values.phone) {
                                    errors.phone = "ກະລຸນາປ້ອນເບີໂທ";
                                }
                                if (!values.password) {
                                    errors.password = "ກະລຸນາປ້ອນລະຫັດຜ່ານ";
                                }


                                return errors;
                            }}
                            onSubmit={async values => {
                                _createRegister(values)
                            }

                            }
                        >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting
                                /* and other goodies */
                            }) => (
                                <Form>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>ຊື່</Form.Label>
                                        <Form.Control type="text" placeholder="ຊື່" value={values.firstName}
                                            name="firstName"
                                            isInvalid={!!errors.firstName}
                                            onChange={handleChange}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.firstName}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>ນາມສະກຸນ</Form.Label>
                                        <Form.Control type="text" placeholder="ນາມສະກຸນ" value={values.lastName}
                                            isInvalid={!!errors.lastName}
                                            onChange={handleChange}
                                            name="lastName"
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.lastName}
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>ເບີໂທລະສັບ</Form.Label>
                                        <Form.Control type="text" placeholder="020 xxxx xxxx" value={values.phone}
                                            isInvalid={!!errors.phone}
                                            onChange={handleChange}
                                            name="phone"
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.phone}
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>ລະຫັດຜ່ານ</Form.Label>
                                        <Form.Control type="password" placeholder="*******" value={values.password}
                                            isInvalid={!!errors.password}
                                            onChange={handleChange}
                                            name="password"
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            {errors.password}
                                        </Form.Control.Feedback>
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                        <Button as={Col} className='save-button' size="lg" onClick={handleSubmit} >ລົງທະບຽນ</Button>
                                    </Form.Group>

                                    <Form.Group className="mb-3">
                                        <p>ຍັງບໍ່ທັນລົງທະບຽນເທື່ອ ? <Link to={Consts.USER_LOGIN}>ເຂົ້າສູ່ລະບົບ</Link></p>
                                    </Form.Group>
                                </Form>
                            )}
                        </Formik>

                    </div>
                    <div className="footer">
                        <p>@2022 ພັດທະນາໂດຍ: ທີມພັດທະນາ ເຈົ້າແຄນເດບ ຄັ້ງ 2</p>
                    </div>

                </div>
                <div className="flex-right">
                    <center>
                        <img
                            className="img"
                            src={logoJCD}
                            alt="First slide"
                        />
                    </center>

                    <p>
                        ເຈົ້າແຄນເດບ ແມ່ນແບຣນໜຶ່ງທີ່ສ້າງຂຶ້ນເພື່ອເນັ້ນໃຫ້ຄວາມຮູ້ທາງດ້ານ ໄອຊີທີ ໃຫ້ນັກນັກຮຽນນັກສຶກສາ ຫຼື ທຸກຄົນໃນ ປະເທດລາວ ເພື່ອຍົກລະດັບ ຂະແໜງການທາງດ້ານ ໄອຊິທີ ໃຫ້ມີຄວາມກ້າວໜ້າ ແລະ ທັນສະໄໝຍິ່ງຂຶ້ນ ບໍ່ວ່າຈະເປັນການຈັດງານອີເວັ້ນຕ່າງໆ ແລະ ກາານສ້າງເປັນອາຄາເດມີ
                    </p>

                    <h4>
                        ງານເຈົ້າແຄນເດບຄັ້ງທີ 2 <br></br> ຈະຈັດຂຶ້ນໃນວັນທີ 26 - 03 - 2022 ທີ່ສູນກິລາ ບຸໂດ ລາວ - ເຈແປນ
                    </h4>


                </div>
            </div>
        </div>
    )
}

export default Register
