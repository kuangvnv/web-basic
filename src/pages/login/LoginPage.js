import React, { useState } from 'react'
import { Link } from "react-router-dom";
import Swal from 'sweetalert2'
import { useNavigate } from "react-router-dom";
import Consts from "../../consts"
import { useMutation } from "@apollo/react-hooks";

import Form from 'react-bootstrap/Form'
import { Button, Row, Col, Carousel } from 'react-bootstrap';
import logoJCD from '../../images/logoJCD.png'
import './Index.css'

import {USER_LOGIN} from "../../apollo/user/Mutation"


function LoginPage() {
  
  const navigate = useNavigate();

  const [userLogin] = useMutation(USER_LOGIN)

  const [phone, setPhone] = useState();
  const [password, setPassword] = useState();

    const defaultOthers = {
        loop: true,
        autoplay: true,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice",
        },
      };

      const _login = async () => {
        try {
          var _data = {
            phone: phone,
            password: password,
          };

    
          const resp = await userLogin({variables:{data:_data}})
          
          if (resp.data.login) {
            localStorage.setItem("ACCESS_TOKEN", resp.data.login.accessToken);
            localStorage.setItem("USER_DATA", JSON.stringify(resp.data.login.data));
            navigate(Consts.PAGE_DESHBOARD);
          }
        } catch (err) {
            Swal.fire({
                icon: 'error',
                title: 'ຊື່ຜູ້ນຳໃຊ້ ຫຼື ລະຫັດຜ່ານ ຜິດ',
                text: 'ກະລຸນາລອງໃໝ່ອີກຄັ້ງ'
              })
        }
      };



  return (
    <div className='login-body'>
      <div className="flex-container">
        <div className="flex-left">
          <h2>Jao Can Dev - ເຈົ້າແຄນເດບ ຄັ້ງທີ 2</h2>

          <div className='form-body'>
            <h3>Login</h3>
            <p>ເຈົ້າແຄນເດບແມ່ນງານທີ່ຈະຊ່ວຍທຸກສະໜັບສະໜູນໄວລຸ້ນຍຸກໃໝ່ໃຫ້ຮູ້ທັນໂລກໄອທີ</p>
            <Form>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>ເບີໂທລະສັບ</Form.Label>
                <Form.Control type="text" placeholder="020 xxxx xxxx"
                 onChange={(e) => {
                  e.preventDefault();
                  setPhone(e.target.value);
                }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>ລະຫັດຜ່ານ</Form.Label>
                <Form.Control type="password" placeholder="*******"
                onChange={(e) => {
                  e.preventDefault();
                  setPassword(e.target.value);
                }}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Button as={Col} className='save-button' size="lg" onClick={() => _login()}>ເຂົ້າສູ່ລະບົບ</Button>
              </Form.Group>

              <Form.Group className="mb-3">
                <p>ຍັງບໍ່ທັນລົງທະບຽນເທື່ອ ? <Link to={Consts.USER_REGISTER}>ລົງທະບຽນບັນຊີໃໝ່</Link></p>
              </Form.Group>
            </Form>
            
          </div>
          <div className="footer">
            <p>@2022 ພັດທະນາໂດຍ: ທີມພັດທະນາ ເຈົ້າແຄນເດບ ຄັ້ງ 2</p>
          </div>

        </div>
        <div className="flex-right">
          <center>
            <img
              className="img"
              src={logoJCD}
              alt="First slide"
            />
          </center>

          <p>
            ເຈົ້າແຄນເດບ ແມ່ນແບຣນໜຶ່ງທີ່ສ້າງຂຶ້ນເພື່ອເນັ້ນໃຫ້ຄວາມຮູ້ທາງດ້ານ ໄອຊີທີ ໃຫ້ນັກນັກຮຽນນັກສຶກສາ ຫຼື ທຸກຄົນໃນ ປະເທດລາວ ເພື່ອຍົກລະດັບ ຂະແໜງການທາງດ້ານ ໄອຊິທີ ໃຫ້ມີຄວາມກ້າວໜ້າ ແລະ ທັນສະໄໝຍິ່ງຂຶ້ນ ບໍ່ວ່າຈະເປັນການຈັດງານອີເວັ້ນຕ່າງໆ ແລະ ກາານສ້າງເປັນອາຄາເດມີ
          </p>

          <h4>
            ງານເຈົ້າແຄນເດບຄັ້ງທີ 2 <br></br> ຈະຈັດຂຶ້ນໃນວັນທີ 26 - 03 - 2022 ທີ່ສູນກິລາ ບຸໂດ ລາວ - ເຈແປນ
          </h4>


        </div>
      </div>
    </div>
  )
}

export default LoginPage
